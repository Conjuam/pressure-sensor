#include <PinChangeInt.h>
#include <PinChangeIntConfig.h>

#include <OWSlave.h>


//    One Wire Slave Data
//                     {Fami, <---, ----, ----, ID--, ----, --->,  CRC} 
unsigned char rom[8] = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00};
//                     {MM0,  MM1,  MM2,  MM3,  NIU0, NIU1, NIU2, NIU3, CRC}
  char scratchpad[9] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};


//  Convertion to mm
#define countsPerMM 1;

//    Pin Layouts
#define L²     0
#define OWPin      2
#define CounterPin 4

// Counter Variable Definition
volatile unsigned long counter    = 0;    

// LED Flash Variables
volatile long    flash       = 0;         // Flash achnowledgement counter
long             flashPause  = 100;       // LED between flash delay
#define          flashLength 50           // Flash length
long             flashStart  = 0;
long             flashStop   = 0;

// OW Interrupt Variables
volatile long prevInt    = 0;      // Previous Interrupt micros
volatile boolean owReset = false;

OWSlave ds(OWPin); 

/********************************************************************************
    Initiate the Environment
********************************************************************************/
void setup() {
  int checkRom();
int sendRom();
int resetValues();
  Serial.begin(9600);
    // Initialise the Pn usage
    //pinMode(LEDPin, OUTPUT); 
    //pinMode(CounterPin, INPUT);
    pinMode(OWPin, INPUT); 

    //digitalWrite(LEDPin, LOW);    

    //attachPcInterrupt(CounterPin,incCounter,FALLING);
     attachInterrupt( 0 ,onewireInterrupt, CHANGE);
     
    //attachInterrupt(OWPin,onewireInterrupt,CHANGE);
    
    // Initialise the One Wire Slave Library
    ds.setRom(rom);

    //flash +=2;
}
//void myInterrupt()
//{
//  Serial.begin(9600);
//  attachInterrupt(0,onewireInterrupt, CHANGE);
//}
/********************************************************************************
    Loop checking for any actions to be taken
********************************************************************************/
void loop() {
//     byte i;
//     byte sendRom[8];
//    //if (flash > 0) FlashLED();    // Control the flashing of the LED
//  Serial.print(sendRom[i], HEX);
//  Serial.print("ROM =");
//  for( i = 0; i < 8; i++) 
//  Serial.write(' ');
////   
//Serial.println(ds.setRom); 
//      ds.setRom = "";
//      Serial.print(addr[i], HEX);
//      Serial.print(":");
    if (owReset) owHandler();     // Handle the OW reset that was received
    delay(100);

}
//************************************************************
// Process One Wire Handling
//************************************************************
void owHandler(void) {

    owReset=false;
    detachInterrupt(OWPin);
    
    if (ds.presence()) {
      // Serial.print('sendRom');
     // Serial.write('rom');
     if (ds.recvAndProcessCmd()) {
     uint8_t cmd = ds.recv();
     if (cmd == 0x44) {
              
              
//                float mm = counter;   // Convert the counter to mm
//                mm /= countsPerMM;    
//                unsigned long mm100 = mm * 100;      // return as mm * 100
//                // Convert the long to 4 bytes
//                scratchpad[0] = mm100 & 0xFF;
//                scratchpad[1] = (mm100 >> 8) & 0xFF;
//                scratchpad[2] = (mm100 >> 16) & 0xFF;
//                scratchpad[3] = (mm100 >> 24) & 0xFF;
//                scratchpad[8] = ds.crc8(scratchpad,8);    // Calculate the CRC
            }
//            if (cmd == 0xBE) {
//                for( int i = 0; i < 9; i++) {
//                    ds.send(scratchpad[i]);
//                }
//               // flash++;
//            }
        }
    }
    attachInterrupt(0,onewireInterrupt,CHANGE);
}

//************************************************************
// Counter Interrupt handling 
//************************************************************
void incCounter(void) {
    counter++;
    flash++;
}

//************************************************************
// Onw WireInterrupt handling 
//************************************************************
void onewireInterrupt(void) {
    volatile long lastMicros = micros() - prevInt;
    prevInt = micros();
    if (lastMicros >= 410 && lastMicros <= 550) { //  OneWire Reset Detected
        owReset=true;
        if (checkRom() == 0) {
    Serial.println("***ARDUINO SLAVE valid for ROM, calculating CRC and sending...");
    Serial.print("****");
    Serial.print((int)rom[0]);
    Serial.print((int)rom[1]);
    Serial.print((int)rom[2]);
    Serial.print((int)rom[3]);
    Serial.print((int)rom[4]);
    Serial.print((int)rom[5]);
    Serial.print((int)rom[6]);
    Serial.println((int)rom[7]);
    sendRom();
    }
}

