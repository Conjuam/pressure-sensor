﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */


/*
 *    Example-Code that emulates a DS18B20
 *
 *    Tested with:
 *    - https://github.com/PaulStoffregen/OneTinyTinyWireMM --> DS18x20-Example, atmega328@16MHz as Slave
 *    - DS9490R-Master, atmega328@16MHz and teensy3.2@96MHz as Slave
 */

#include "OneWireHub.h"
#include <TinyWireM.h>
#include <USI_TWI_Master.h>
#include "DS18B20.h"  // Digital Thermometer, 12bit
//Beginning of Auto generated function prototypes by Atmel Studio
//End of Auto generated function prototypes by Atmel Studio


byte sensorData[15]; //Array to hold data from I2C sensor


constexpr uint8_t pin_led       { 13 };
constexpr uint8_t pin_oneWire   { 8 };

auto hub    = OneWireHub(pin_oneWire);

auto ds18b20 = DS18B20(DS18B20::family_code, 0x00, 0x00, 0xB2, 0x18, 0xDA, 0x00); // DS18B20: 9-12bit, -55 -  +85 degC
auto ds18s20 = DS18B20(0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02);                 // DS18S20: 9   bit, -55 -  +85 degC
auto ds1822  = DS18B20(0x28, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02);                 // DS1822:  9-12bit, -55 - +125 degC

bool blinking(void);

void setup()
{
    TinyWireM.begin();
    //Serial.begin(115200);
    //Serial.println("OneTinyTinyWireMM-Hub Emulated DS18B20 Reading And Transmitting Data");
    //Serial.println("From Sensiron SDP810-500Pa Differential Pressure Sensor");
    //Serial.flush();

    //Serial.println("Communicating With Pressure Sensor...");
    TinyWireM.beginTransmission(0x25);
    TinyWireM.write(0x36);
    TinyWireM.write(0x1E);
    TinyWireM.endTransmission();
    //Serial.println("Communication Complete");

    pinMode(pin_led, OUTPUT);

    // Setup OneWire
    //Serial.println("Configuring OneWire Protocol");
    hub.attach(ds18b20);
    hub.attach(ds18s20);
    hub.attach(ds1822);
    //Serial.println("OneWire Connection Established");

    //Serial.println("Self Test & Calibration Starting");
    //Test-Cases: the following code is just to show basic functions, can be removed any time
    //Serial.print("Test - set Temperatures to -56 degC (out of range): ");
    ds18b20.setTemperature(int8_t(-56));
    //Serial.println(ds18b20.getTemperature());
    
    //Serial.print("Test - set Temperatures to -55 degC: ");
    ds18b20.setTemperature(int8_t(-55));
    ds18s20.setTemperature(int8_t(-55));
    //Serial.print(ds18b20.getTemperature());
    //Serial.print(", ");
    //Serial.println(ds18s20.getTemperature());   // ds18s20 is limited to signed 9bit, so it could behave different
    
    //Serial.print("Test - set Temperatures to 0 degC: ");
    ds18b20.setTemperature(int8_t(0));
    ds18s20.setTemperature(int8_t(0));
    //Serial.print(ds18b20.getTemperature());
    //Serial.print(", ");
    //Serial.println(ds18s20.getTemperature());
    
    //Serial.print("Test - set Temperatures to 21 degC: ");
    const int8_t temperature = 21;
    ds18b20.setTemperature(temperature);
    ds18s20.setTemperature(temperature);
    ds1822.setTemperature(temperature);
    //Serial.print(ds18b20.getTemperature());
    //Serial.print(", ");
    //Serial.println(ds18s20.getTemperature());
    
    //Serial.print("Test - set Temperatures to 85 degC: ");
    ds18b20.setTemperature(int8_t(85));
    ds18s20.setTemperature(int8_t(85));
    //Serial.print(ds18b20.getTemperature());
    //Serial.print(", ");
    //Serial.println(ds18s20.getTemperature());
    
    //Serial.print("Test - set Temperatures to 126 degC (out of range): ");
    ds1822.setTemperature(int8_t(126));
    //Serial.println(ds1822.getTemperature());
    delay(1000);
    //Serial.println("Self-Test Complete Successfully!");
    
    delay(1000);
    
}

void loop()
{
    TinyWireM.requestFrom(0x25, 9);
    for(int i=0; i<9; i++)
    {
      sensorData[i] = TinyWireM.read();
    }
    delay(1);
    
    int16_t Pressure_raw = sensorData[0]<<8|sensorData[1]; //Raw Pressure
    //sensorData[2] = CRC8 of pressure data

    int16_t Temperature_raw = sensorData[3]<<8|sensorData[4]; //Raw Temperature
    //sensorData[5] = CRC8 of temperature data

    int16_t SF_pressure = sensorData[6]<<8|sensorData[7]; //Scale factor for differential pressure
    //sensorData[8] = CRC8 of scale factor data
    double Pressure = Pressure_raw/SF_pressure;

    int PressurePWM = (((Pressure/500)+1)-2);

    
    
    // following function must be called periodically
    hub.poll();
    // this part is just for debugging (USE_//Serial_DEBUG in OneWire.h must be enabled for output)
    if (hub.hasError()) hub.printError();

    // Blink triggers the state-change
    if (blinking())
    {
        // Set temp
        static float temperature = 20.0;
        temperature += 0.1;
        if(temperature > 30.0) temperature = 20.0;
        ds18b20.setTemperature(temperature);
        ds18s20.setTemperature(temperature);
        ds1822.setTemperature(temperature);
        //Serial.println(temperature);
    }
}

bool blinking(void)
{
    constexpr  uint32_t interval    = 1000;          // interval at which to blink (milliseconds)
    static uint32_t nextMillis  = millis();     // will store next time LED will updated

    if (millis() > nextMillis)
    {
        nextMillis += interval;             // save the next time you blinked the LED
        static uint8_t ledState = LOW;      // ledState used to set the LED
        if (ledState == LOW)    ledState = HIGH;
        else                    ledState = LOW;
        digitalWrite(pin_led, ledState);
        return 1;
    }
    return 0;
}
