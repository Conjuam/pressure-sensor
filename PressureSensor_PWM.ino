#include <Wire.h>
byte sensorData[15]; //Array to hold data from sensor

const int pwmPin = 6;

void setup()
{
  Wire.begin();
  Serial.begin(9600);

 
   Wire.beginTransmission(0x25);
   Wire.write(0x36);
   Wire.write(0x1E);
   Wire.endTransmission();

  /* 
   *  Continuous Reading Commands
   *  
   *  Mass Flow, Avg till read
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3603);
   *  Wire.endTransmission();
   * 
   *  Mass Flow, No Averaging, update every 0.5 secs
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3608);
   *  Wire.endTransmission();
   * 
   *  Diff. Pressure, Avg till read
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3615);
   *  Wire.endTransmission();
   *  
   *  Diff. Pressure, No Averaging, update every 0.5 secs
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x361E)
   *  Wire.endTransmission();
   *  
   *  ---------------------------------------------------------
   *  
   *  Triggered Reading Commands
   *  
   *  Mass Flow, No Clock Stretching
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3624);
   *  Wire.endTransmission();
   *  
   *  Mass Flow w/ Clock Stretching
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3726);
   *  Wire.endTransmission();
   * 
   *  Diff. Pressure, No Clock Stretching
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x362F);
   *  Wire.endTransmission();
   *  
   *  Diff. Pressure w/ Clock Stretching
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x272D);
   *  Wire.endTransmission();
   *  
   *  ---------------------------------------------------------
   *  
   *  Reset CMD
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x0006);
   *  Wire.endTransmission();
   *  
   *  ---------------------------------------------------------
   *  
   *  Enter Sleep Mode
   *  Wire.beginTransmission(0x25);
   *  Wire.write(0x3677);
   *  Wire.endTransmission();
   *  Any other command will wake the sensor from this mode.
   */

  delay(5);
}

void loop()
{

  Wire.requestFrom(0x25, 9);
  for(int i=0; i<9; i++)
  {
    sensorData[i] = Wire.read();
  }
  delay(1);


  int16_t Pressure_raw = sensorData[0]<<8|sensorData[1]; //Raw Pressure
  //sensorData[2] = CRC of Pressure Data

  int16_t Temperature_raw = sensorData[3]<<8|sensorData[4]; //Raw Temp
  //sensorData[5] = CRC of Temp. Data
  double Temperature = Temperature_raw/float(200);

  float SF_pressure = sensorData[6]<<8|sensorData[7]; //Scale factor for Diff. Pressure
  //sensorData[8] = CRC of SF Data
  double Pressure = Pressure_raw/SF_pressure;
  
  int PressurePWM = (((Pressure/500)+1)/2);


  /*---Measurement stop CMD---
   * Wire.beginTransmission(0x25);
   * Wire.write(0x3FF9);
   * Wire.endTransmission();
   */

   Serial.print("Differential Pressure (RAW) = ");
   Serial.print(PressurePWM);
   Serial.print("\n");
   //Serial.print(Pressure,5); //Pressure in pascals
   //Serial.print(" Pascals\n");

   //Serial.print("Temperature = ");
   //Serial.print(Temperature);
   //Serial.print(" °C\n");
   //Serial.print("\n");

   if(PressurePWM = 0.00)
   {
    analogWrite(pwmPin, 0);
   }
   else(PressurePWM !=0.00);
   {
    analogWrite(pwmPin, PressurePWM*255);
   }

   delay(500);
}
